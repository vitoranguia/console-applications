# Console applications

My favorite console applications

Console emulator

```
# apt install -y xterm
```

Virtual console

```
# apt install -y screen
```

Copy and past

```
# apt install -y xclip
```

Archive and compress

```
# apt install -y zip unzip unrar gzip bzip2 xz-utils
```

Network manager (run `nmtui`)

```
# apt install -y network-manager
```

File manager

```
# apt install -y mc
```

Password manager (run `keepassxc-cli`)

```
# apt install -y keepassxc
```

Text editor

```
# apt install -y vim universal-ctags
```

Text converter

```
# apt install -y pandoc
```

Image converter (run `convert`)

```
# apt install -y imagemagick
```

Audio manager (run `alsamixer`)

```
# apt install -y alsa-utils
```

Audio and video converter

```
# apt install -y ffmpeg
```

Video player (run `ffplay`)

```
# apt install -y ffmpeg
```

Audio player

```
# apt install -y cmus
```

Podcast player (run `gpo`)

```
# apt install -y gpodder
```

BitTorrent

```
# apt install -y transmission-daemon transmission-cli transmission-remote-cli
```

Web browser

```
# apt install -y lynx
```

IMAP sync

```
# apt install -y offlineimap
```

Mail client (see `offlineimap` and `khard`)

```
# apt install -y mutt urlview
```

IRC client

```
# apt install -y irssi
```

XMPP client

```
# apt install -y profanity
```

NextCloud CLI (see `NextCloud`)

```
# apt install -y nextcloud-desktop-cmd
```

News read (see `NextCloud News`)

```
# apt install -y newsboat
```

WebDav (see `NextCloud`)

```
# apt install -y davfs2
```

CalDav and CardDav sync (see `NextCloud`)

```
# apt install -y vdirsyncer
```

Calendar (see `vdirsyncer`)

```
# apt install -y khal
```

Contacts (see `vdirsyncer`)

```
# apt install -y khard
```

Todo list (see `vdirsyncer`)

```
# apt install -y todoman
```

Weather forecast

```
$ curl wttr.in/?format=4
```

System information

```
# apt install -y htop screenfetch
```

Screensaver

```
# apt install -y cmatrix
```

QR Code

```
# apt install -y qrencode
```


Funny

```
# apt install -y fortune-mod cowsay lolcat figlet espeak
```
